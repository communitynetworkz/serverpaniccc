<?php
namespace Serverpokedalarm\Classes;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Small log rotation for error_log debug construct.
 *
 * In php the command "error_log('some text, for instance')" will write 
 * a log-entry to a file indicated with ini_set("error_log","pathtofile"). 
 * When used for debug purposes it can result in very large filesizes, 
 * especially when forgotten and put into a production environment. This 
 * class tries to prevent problems by controlling this filesize via a very small 
 * logrotation strategie (1 backup file). 
 * 
 * @author wouter
 * @todo Tjs , wat nog te doen parameter $logerors: could be an array or string to set the error level
 */
class Errorlogrecord {
    //put your code here
    protected $dirpath;//the path of the file instantiating this class
    protected $filepath;//the path of the file we are monitoring
    protected $filename;//name of the file we are monitoring
    protected $logcondition;//when to use this class: when to log error_log() commands
    protected $logdirpath;//path to directory where log-files are created and added to
    protected $maxfsize;//maximal log file size before rotate action
    protected $logfileexists;//logfile does exist
    protected $logfilepath;//path to log file
    protected $logfiletobig;//boolean indicating whether rotation is needed
    protected $linenumber;//for default log contribution
    protected $logerrors;//only log from error_log() function (false) or also log php errors (true)
    protected $logdirpathok;//true: log can be written to, false: there is a problem writing to log

    /**
     * 
     * @param string|null $dirpath The path of "this" file that references this class
     * @param string|boolean|null $logcondition A string containing an statement that evaluates to true or false: for instantance $cookie['woutertester'=="woutertester", or simply true]
     * @param string|null $logdirpath The path to the directory where logfiles will be created
     * @param int|null $linenumber For instance the linenumber of the new() for this class
     * @param boolean logerrors true if php errors need to be logged to
     */
   //function __construct($filepath,$dirpath,$logcondition,$logdirpath,$linenumber,$logerrors,$maxfsize) {
  function __construct($attrarray) {
        //set properties: 
        $propertiesset =$this->setPropertiesWithArguments($attrarray['filepath'],dirname($attrarray['filepath']),$attrarray['logcondition'],$attrarray['logdirpath'],$attrarray['linenumber'],$attrarray['logerrors'],$attrarray['maxfsize']);
        //now initialize things
        $initialized = $this->initialize();
        //echo ":::::::::::::" . $this->logdirpathok . "==============";
        if($propertiesset&&$initialized&&$this->logcondition&&$this->logdirpathok){//we want, and can write to / create log file
            //echo "okdandoehetmaar";
            //first see if log file is full, and if so
            //"rotate" file 
            if($this->logfiletobig){
                //echo "to big!!!";
                $rotated =$this->rotate();
            }
            //now do the thing
           // ini_set('display_errors',0);
            if($this->logerrors==true){
                
                error_reporting(-1);
                //error_reporting(E_ALL);
            }else{
                error_reporting(0);
            }
            ini_set("log_errors",1);
            //echo "\n\n" . $this->logfilepath . "\n\n";
            ini_set("error_log", $this->logfilepath);
            $filistamp = "line ".$this->linenumber;//file line stamp ;-)
            error_log($filistamp);
        }else{
            echo "no error_log logging";//???
        }

        
    }
    /**
     * 
     * @return boolean
     */
    protected function initialize(){
        if($this->testLogdirpath()){
            $this->logdirpathok = true;
        }else{
            $this->logdirpathok = false;
        }
        if(touch($this->logfilepath)){//creates files if not existing
            chmod($this->logfilepath, 0775);//u+rwx,g+rwx,o+rx see http://permissions-calculator.org/
            $this->logfileexists=true;
        }else{
            $this->logfileexists=false;
        }
        if($this->logdirpathok&&$this->testLogfileSize($this->maxfsize)){
            $this->logfiletobig = true;
        }else{
            $this->logfiletobig = false;
        }
        return true;
    }

    /**
     * 
     * @param string $filepath
     * @param string $dirpath
     * @param boolean|string $logcondition
     * @param string $logdirpath
     * @param int $linenumber
     * @param boolean $logerrors
     * @return boolean
     */
    protected function setPropertiesWithArguments($filepath,$dirpath,$logcondition,$logdirpath,$linenumber,$logerrors,$maxfsize){
        if(isset($linenumber)&&$linenumber!=''){
            $this->linenumber= $linenumber;
        }else{
            $this->linenumber= __LINE__;
        }
        if(isset($logcondition)&&$logcondition!=''){
            $this->logcondition= $logcondition;
        }else{
            $this->logcondition= true;
        }
        if(isset($filepath)&&$filepath!=''){
            $this->filepath = $filepath;
            $this->filename=  basename($filepath);
        }else{
            $this->filepath = __FILE__;
            $this->filename=  basename(__FILE__);
        }
        if(isset($dirpath)&&$dirpath!=''){
            $this->dirpath = $dirpath;
        }else{
            $this->dirpath = __DIR__;
        }
        if(isset($logdirpath)&&$logdirpath!=''){
            if(mb_ereg('\A/',$logdirpath)){//is there a leading slash: then it is an absolute path
                $this->logdirpath = $logdirpath;
                //echo "dislogdirpad: ". $this->logdirpath;
            }else{//an relative path
                $this->logdirpath = $this->dirpath."/".$logdirpath;
            }
            
        }else{
            $this->logdirpath = $this->dirpath."/phperrorlogs";
        }
        if(isset($logerrors)&&$logerrors!=''){
            $this->logerrors = $logerrors;
        }else{
            $this->logerrors = false;
        }
        if(isset($maxfsize)&&$maxfsize!=''&& is_numeric($maxfsize)){
            $this->maxfsize = $maxfsize;
        }else{
            $this->maxfsize = 100000;
        }
        // build path to log file
        $this->logfilepath = $this->logdirpath . "/". $this->filename . ".log";
        //ready now: no problems are expected
        return true;
    }
    /**
     * Test log file size.
     * 
     * Test log file size
     * @
     * @param int $maxfsize The max size in bytes of file
     * 
     * @return boolean
     */
    protected function testLogfileSize($maxfsize=100000){//1000000 == max about 100K
        //echo $logfilepath = $this->logdirpath . "/".$this->filename . ".log";echo "\n";
        echo "maxsizenu:: ".$maxfsize . "\n\n";
        $logfilepath = $this->logfilepath;
        if(is_file($logfilepath) && filesize($logfilepath)>(int)$maxfsize){
            return true;
        }else{
            //echo "no file or to small";
            //fill it up for testing 
            //echo "maxfsize:::".$maxfsize;
            //echo "filesizea:::".filesize($logfilepath);
            //if(filesize($logfilepath)>$maxfsize){echo"AAAAAAAAAAAAAAAAAAAAAAAAAA";};
           while(false){//deze werkt niet: gaat op hol: misschien omdat filesize() niet compatible is met dis i/o cache?
               //break;
               sleep(1);//nodig??
                 clearstatcache();//lijkt te werken hiermee : 
               if(filesize($logfilepath)>$maxfsize){echo"AAAAAAAAAAAAAAAAAAAAAAAAAA";break;}
               else{
                   file_put_contents($logfilepath, "blablablablabla\n", FILE_APPEND);
               };
                
                if(filesize($logfilepath)>$maxfsize){echo"AAAAAAAAAAAAAAAAAAAAAAAAAA";break;};
            }
            //echo "filesizeb:::".filesize($logfilepath);
            return false;
        }
        
    }
    
    /**
     * Rotate log file.
     * 
     * Rotate log file
     * 
     * @return boolean
     */
    protected function rotate(){
        $logfilepath = $this->logfilepath;
        //if(is_file($logfilepath) && (filesize($logfilepath)>(int)$maxfsize)){
            file_put_contents($logfilepath, "endaction:filetobig:needtoreload tail perhaps ".time() . "\n", FILE_APPEND);
            if(is_file($logfilepath.".backup")){
                unlink($logfilepath.".backup");
            }
            copy($logfilepath,$logfilepath.".backup");
            chmod($logfilepath. ".backup", 0775);
            file_put_contents($logfilepath,'Emptied timestamp:  ' . time()."\n");
            
        //}
        return true;
    }
    /**
     * @todo mmmmmmmmmmm, why create all these getters and not use them?
     * @return int Line number
     */
    public function getLinenumber(){
        return (int)$this->linenumber;
    }
    /**
     * 
     * @return string Path to directory of "this" file
     */
    public function getDirpath(){
        return $this->dirpath;
    }
     /**
     * 
     * @return string The file path of "this" file that references this class/method
     */
    public function getFilepath(){
        return $this->filepath;
    }
    /**
     * 
     * @return string Path to directory where log files are written 
     */
    public function getLogdirpath(){
        return $this->logdirpath;
    }
    /**
     * 
     * @return boolean
     */
    function getLogcondition(){
        return $this->logcondition;
    }
    /**
     * 
     * @return boolean
     */
    protected function testLogdirpath(){
        $logdirpath=$this->logdirpath;
        if(is_dir($logdirpath)&&is_writable($logdirpath)){
            return true;
        }else {
            return false;
        }


    }
}
